﻿using System;
using System.Data;
using System.Text;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.TextFile
{
    internal partial class StTextReader_V
        : DsEditForm_V, StTextReader_VI
    {
        private EventHandler okayButton_Click;

        private EventHandler cancelButton_Click;

        private DataTable encodingTable = new DataTable();

        private DataTable readingTypeTable = new DataTable();

        public StTextReader_V()
        {
            this.InitializeComponent();
        }

        public Action OkButton_Click
        {
            set
            {
                this.okButton.Click -= this.okayButton_Click;
                this.okayButton_Click = (sender, e) => value();
                this.okButton.Click += this.okayButton_Click;
            }
        }

        public Action CancelButton_Click
        {
            set
            {
                this.cancelButton.Click -= this.cancelButton_Click;
                this.cancelButton_Click = (sender, e) => value();
                this.cancelButton.Click += this.cancelButton_Click;
            }
        }

        public String Message
        {
            set { this.statusLabel.Text = value; }
        }

        public Encoding Encoding
        {
            get { return this.encodingComboBox.SelectedValue as Encoding; }
            set { this.encodingComboBox.SelectedValue = value; }
        }

        public MdTextReader.ReadingType ReadingType
        {
            get { return (MdTextReader.ReadingType)this.readingTypeComboBox.SelectedValue; }
            set { this.readingTypeComboBox.SelectedValue = value; }
        }

        public override void Initialize()
        {
            this.InitializeEncodingComboBox();
            this.InitializeReadingTypeComboBox();
        }

        private void InitializeEncodingComboBox()
        {
            this.encodingTable = TextFileUtils.CreateEncodingTable();

            this.encodingComboBox.DataSource = this.encodingTable;
            this.encodingComboBox.DisplayMember = TextFileUtils.EncodingTable.Name;
            this.encodingComboBox.ValueMember = TextFileUtils.EncodingTable.Encoding;
        }

        private void InitializeReadingTypeComboBox()
        {
            this.readingTypeTable = TextFileUtils.CreateReadingTypeTable();

            this.readingTypeComboBox.DataSource = this.readingTypeTable;
            this.readingTypeComboBox.DisplayMember = TextFileUtils.ReadingTypeTable.Name;
            this.readingTypeComboBox.ValueMember = TextFileUtils.ReadingTypeTable.ReadingType;
        }
    }
}
