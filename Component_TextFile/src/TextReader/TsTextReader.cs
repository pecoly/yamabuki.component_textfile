﻿using System;
using System.IO;
using System.Text;

using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Utility;

namespace Yamabuki.Component.TextFile
{
    internal class TsTextReader
        : TsTask_1_1
    {
        private Encoding encoding;

        private MdTextReader.ReadingType readingType;

        public TsTextReader(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid,
            Encoding encoding,
            MdTextReader.ReadingType readingType)
            : base(definitionPath, inputDataStoreGuid, outputDataStoreGuid)
        {
            this.encoding = encoding;
            this.readingType = readingType;
        }

        protected override void CheckInputDataList(TsDataList inputDataList)
        {
            var convertedInputDataList = TsAppContext.AutoCastEnabled ?
                inputDataList.Convert(typeof(String)) : inputDataList;

            TsDataTypeCheckUtils.Check(this.DefinitionPath, typeof(String), convertedInputDataList.Type);
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            outputDataList = new TsDataListT<String>();
        }

        protected override void ExecuteInternal(TsDataList inputDataList, TsDataList outputDataList)
        {
            if (inputDataList.Count != 1)
            {
                throw new TsInputDataInvalidLengthException(
                    1, this.DefinitionPath, TextFileWriterConstants.InputPortIndex_FilePath);
            }

            var convertedInputDataList = TsAppContext.AutoCastEnabled ?
                inputDataList.Convert(typeof(String)) : inputDataList;

            var convertedInputDataListT = convertedInputDataList as TsDataListT<String>;
            var outputDataListT = outputDataList as TsDataListT<String>;

            var filePath = convertedInputDataListT.GetValue(0);

            this.WriteLog(Core.LogLevel.Info, this.DefinitionPath + " " + filePath + "から読み込みます。");

            if (this.readingType == MdTextReader.ReadingType.ReadLines)
            {
                foreach (var x in File.ReadAllLines(filePath, this.encoding))
                {
                    outputDataListT.AddValue(x);
                }
            }
            else if (this.readingType == MdTextReader.ReadingType.ReadAllText)
            {
                outputDataListT.AddValue(File.ReadAllText(filePath, this.encoding));
            }
        }

        protected virtual StreamWriter CreateStreamReader(String filePath, Encoding encoding)
        {
            return new StreamWriter(filePath, false, encoding);
        }

        private struct TextFileWriterConstants
        {
            public static readonly Int32 InputPortIndex_FilePath = 0;
        }
    }
}
