﻿using System;
using System.Diagnostics;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.TextFile
{
    internal class StTextReader_P
        : DsEditForm_P<StTextReader_VI, DsTextReader>
    {
        public StTextReader_P(DsTextReader com)
            : base(com)
        {
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.Encoding = this.Component.Encoding;
            this.View.ReadingType = this.Component.ReadingType;
        }

        public override void Save()
        {
            this.Component.Encoding = this.View.Encoding;
            this.Component.ReadingType = this.View.ReadingType;
        }
    }
}
