﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yamabuki.Component.TextFile
{
    public class MdTextReader
    {
        public enum ReadingType
        {
            ReadLines,

            ReadAllText,
        }

        public static String ToString(ReadingType type)
        {
            switch (type)
            {
                case ReadingType.ReadLines:
                    return "ReadLines";
                case ReadingType.ReadAllText:
                    return "ReadAllText";
                default:
                    return "";
            }
        }

        public static ReadingType ToReadingType(String value)
        {
            switch (value)
            {
                case "ReadLines":
                    return ReadingType.ReadLines;
                case "ReadAllText":
                    return ReadingType.ReadAllText;
                default:
                    return ReadingType.ReadLines;
            }
        }
    }
}
