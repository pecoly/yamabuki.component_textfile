﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.Message;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Utility.Log;
using Yamabuki.Window.Core;

namespace Yamabuki.Component.TextFile
{
    public class DsTextReader
        : DsSimpleComponent_1_1
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public override int DefaultWidth
        {
            get { return 144; }
        }

        protected override String InputName0
        {
            get { return "ファイルパス"; }
        }

        protected override String OutputName0
        {
            get { return "データ"; }
        }

        internal Encoding Encoding { get; set; }

        internal MdTextReader.ReadingType ReadingType{ get; set; }

        public override void Initialize(IEnumerable<XElement> data)
        {
            this.Encoding = Encoding.UTF8;
            this.ReadingType = MdTextReader.ReadingType.ReadLines;

            foreach (var e in data)
            {
                this.InitializeParameter(e.Name.LocalName, e.Value);
            }
        }

        public override BaseMessage DoubleClick()
        {
            var oldE = this.GetXElement();
            var result = this.ShowDialog();

            var isUpdated = result == FormResult.Ok;
            if (!isUpdated)
            {
                return null;
            }

            return new UpdateComponentMessage(oldE, this.GetXElement());
        }

        public override void SetParameter(IDictionary<String, String> paramList)
        {
            foreach (var kv in paramList)
            {
                this.InitializeParameter(kv.Key, kv.Value);
            }
        }

        public override IEnumerable<XElement> DataToXElement()
        {
            var list = new List<XElement>();
            list.Add(new XElement(Property.Encoding, this.Encoding.CodePage));
            list.Add(new XElement(Property.ReadingType, MdTextReader.ToString(this.ReadingType)));
            return list;
        }
        
        protected override void Initialize_1_1()
        {
            this.Encoding = Encoding.UTF8;
            this.ReadingType = MdTextReader.ReadingType.ReadLines;
        }

        protected virtual FormResult ShowDialog()
        {
            using (var presenter = new StTextReader_P(this))
            using (var view = new StTextReader_V())
            {
                presenter.View = view;
                return view.ShowModal();
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsTextReader(
                this.DefinitionPath,
                inputDataStoreGuid,
                outputDataStoreGuid,
                this.Encoding,
                this.ReadingType);
        }

        internal void InitializeParameter(String key, String value)
        {
            if (key == Property.Encoding)
            {
                this.Encoding = TextFileUtils.StringToEncoding(value);
            }
            else if (key == Property.ReadingType)
            {
                this.ReadingType = MdTextReader.ToReadingType(value);
            }
        }

        /// <summary>プロパティ</summary>
        private struct Property
        {
            internal const String Encoding = "Encoding";
            internal const String ReadingType = "ReadingType";
        }
    }
}
