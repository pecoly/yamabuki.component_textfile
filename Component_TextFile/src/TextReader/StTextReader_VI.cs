﻿using System;
using System.Text;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.TextFile
{
    internal interface StTextReader_VI
        : DsEditForm_VI
    {
        Encoding Encoding { get; set; }

        MdTextReader.ReadingType ReadingType { get; set; }
    }
}
