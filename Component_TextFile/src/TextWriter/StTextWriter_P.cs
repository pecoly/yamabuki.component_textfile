﻿using System;
using System.Diagnostics;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.TextFile
{
    internal class StTextWriter_P
        : DsEditForm_P<StTextWriter_VI, DsTextWriter>
    {
        public StTextWriter_P(DsTextWriter com)
            : base(com)
        {
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.Encoding = this.Component.Encoding;
            this.View.WritingType = this.Component.WritingType;
        }

        public override void Save()
        {
            this.Component.Encoding = this.View.Encoding;
            this.Component.WritingType = this.View.WritingType;
        }
    }
}
