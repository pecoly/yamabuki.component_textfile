﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.Message;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Utility.Cast;
using Yamabuki.Utility.Log;
using Yamabuki.Window.Core;

namespace Yamabuki.Component.TextFile
{
    public class DsTextWriter
        : DsSimpleComponent_2_0
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public override int DefaultWidth
        {
            get { return 144; }
        }

        protected override String InputName0
        {
            get { return "ファイルパス"; }
        }

        protected override String InputName1
        {
            get { return "データ"; }
        }

        internal Encoding Encoding { get; set; }

        internal MdTextWriter.WritingType WritingType { get; set; }

        public override void Initialize(IEnumerable<XElement> data)
        {
            this.Encoding = Encoding.UTF8;
            this.WritingType = MdTextWriter.WritingType.WriteLines;

            foreach (var e in data)
            {
                this.InitializeParameter(e.Name.LocalName, e.Value);
            }
        }

        public override BaseMessage DoubleClick()
        {
            var oldE = this.GetXElement();
            var result = this.ShowDialog();

            var isUpdated = result == FormResult.Ok;
            if (!isUpdated)
            {
                return null;
            }

            return new UpdateComponentMessage(oldE, this.GetXElement());
        }

        public override void SetParameter(IDictionary<String, String> paramList)
        {
            foreach (var kv in paramList)
            {
                this.InitializeParameter(kv.Key, kv.Value);
            }
        }

        public override IEnumerable<XElement> DataToXElement()
        {
            var list = new List<XElement>();
            list.Add(new XElement(Property.Encoding, this.Encoding.CodePage));
            list.Add(new XElement(Property.WritingType, MdTextWriter.ToString(this.WritingType)));
            return list;
        }
        
        protected override void Initialize_2_0()
        {
            this.Encoding = Encoding.UTF8;
            this.WritingType = MdTextWriter.WritingType.WriteLines;
        }

        protected virtual FormResult ShowDialog()
        {
            using (var presenter = new StTextWriter_P(this))
            using (var view = new StTextWriter_V())
            {
                presenter.View = view;
                return view.ShowModal();
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1)
        {
            return new TsTextWriter(
                this.DefinitionPath,
                inputDataStoreGuid0,
                inputDataStoreGuid1,
                this.Encoding,
                this.WritingType);
        }
        
        internal void InitializeParameter(String key, String value)
        {
            if (key == Property.Encoding)
            {
                this.Encoding = TextFileUtils.StringToEncoding(value);
            }
            else if (key == Property.WritingType)
            {
                this.WritingType = MdTextWriter.ToWritingType(value);
            }
        }
        /// <summary>プロパティ</summary>
        private struct Property
        {
            internal const String Encoding = "Encoding";
            internal const String WritingType = "WritingType";
        }
    }
}
