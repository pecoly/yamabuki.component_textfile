﻿using System;
using System.Data;
using System.Text;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.TextFile
{
    internal partial class StTextWriter_V
        : DsEditForm_V, StTextWriter_VI
    {
        private EventHandler okayButton_Click;

        private EventHandler cancelButton_Click;

        private DataTable encodingTable = new DataTable();

        private DataTable writingTypeTable = new DataTable();

        public StTextWriter_V()
        {
            this.InitializeComponent();
        }

        public Action OkButton_Click
        {
            set
            {
                this.okButton.Click -= this.okayButton_Click;
                this.okayButton_Click = (sender, e) => value();
                this.okButton.Click += this.okayButton_Click;
            }
        }

        public Action CancelButton_Click
        {
            set
            {
                this.cancelButton.Click -= this.cancelButton_Click;
                this.cancelButton_Click = (sender, e) => value();
                this.cancelButton.Click += this.cancelButton_Click;
            }
        }

        public String Message
        {
            set { this.statusLabel.Text = value; }
        }

        public Encoding Encoding
        {
            get { return this.encodingComboBox.SelectedValue as Encoding; }
            set { this.encodingComboBox.SelectedValue = value; }
        }

        public MdTextWriter.WritingType WritingType
        {
            get { return (MdTextWriter.WritingType)this.writingTypeComboBox.SelectedValue; }
            set { this.writingTypeComboBox.SelectedValue = value; }
        }

        public override void Initialize()
        {
            this.InitializeEncodingComboBox();
            this.InitializeWritingTypeComboBox();
        }

        private void InitializeEncodingComboBox()
        {
            this.encodingTable = TextFileUtils.CreateEncodingTable();

            this.encodingComboBox.DataSource = this.encodingTable;
            this.encodingComboBox.DisplayMember = TextFileUtils.EncodingTable.Name;
            this.encodingComboBox.ValueMember = TextFileUtils.EncodingTable.Encoding;
        }

        private void InitializeWritingTypeComboBox()
        {
            this.writingTypeTable = TextFileUtils.CreateWritingTypeTable();

            this.writingTypeComboBox.DataSource = this.writingTypeTable;
            this.writingTypeComboBox.DisplayMember = TextFileUtils.WritingTypeTable.Name;
            this.writingTypeComboBox.ValueMember = TextFileUtils.WritingTypeTable.WritingType;
        }
    }
}
