﻿using System;
using System.IO;
using System.Text;

using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Utility;

namespace Yamabuki.Component.TextFile
{
    internal class TsTextWriter
        : TsTask_2_0
    {
        private Encoding encoding;

        private MdTextWriter.WritingType writingType;

        public TsTextWriter(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            Encoding encoding,
            MdTextWriter.WritingType writingType)
            : base(definitionPath, inputDataStoreGuid0, inputDataStoreGuid1)
        {
            this.encoding = encoding;
            this.writingType = writingType;
        }

        protected override void CheckInputDataList(TsDataList inputDataList0, TsDataList inputDataList1)
        {
            var convertedInputDataList0 = TsAppContext.AutoCastEnabled ?
                inputDataList0.Convert(typeof(String)) : inputDataList0;

            TsDataTypeCheckUtils.Check(this.DefinitionPath, typeof(String), convertedInputDataList0.Type);

            var convertedInputDataList1 = TsAppContext.AutoCastEnabled ?
                inputDataList1.Convert(typeof(String)) : inputDataList1;

            TsDataTypeCheckUtils.Check(this.DefinitionPath, typeof(String), convertedInputDataList1.Type);
        }

        protected override void ExecuteInternal(TsDataList inputDataList0, TsDataList inputDataList1)
        {
            if (inputDataList0.Count != 1)
            {
                throw new TsInputDataInvalidLengthException(
                    1, this.DefinitionPath, TextFileWriterConstants.InputPortIndex_FilePath);
            }

            var convertedInputDataList0 = TsAppContext.AutoCastEnabled ?
                inputDataList0.Convert(typeof(String)) : inputDataList0;

            var convertedInputDataList1 = TsAppContext.AutoCastEnabled ?
                inputDataList1.Convert(typeof(String)) : inputDataList1;

            var convertedInputDataListT0 = convertedInputDataList0 as TsDataListT<String>;
            var convertedInputDataListT1 = convertedInputDataList1 as TsDataListT<String>;

            var filePath = convertedInputDataListT0.GetValue(0);

            this.WriteLog(Core.LogLevel.Info, this.DefinitionPath + " " + filePath + "へ書き込みます。");

            if (this.writingType == MdTextWriter.WritingType.WriteLines)
            {
                using (var writer = this.CreateStreamWriter(filePath, this.encoding))
                {
                    foreach (var x in convertedInputDataListT1.ValueList)
                    {
                        writer.WriteLine(x);
                    }
                }
            }
            else if (this.writingType == MdTextWriter.WritingType.WriteAllText)
            {
                using (var writer = this.CreateStreamWriter(filePath, this.encoding))
                {
                    foreach (var x in convertedInputDataListT1.ValueList)
                    {
                        writer.Write(x);
                    }
                }
            }
        }

        protected virtual StreamWriter CreateStreamWriter(String filePath, Encoding encoding)
        {
            return new StreamWriter(filePath, false, encoding);
        }

        private struct TextFileWriterConstants
        {
            public static readonly Int32 InputPortIndex_FilePath = 0;
        }
    }
}
