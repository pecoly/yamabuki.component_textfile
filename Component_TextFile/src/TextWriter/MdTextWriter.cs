﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yamabuki.Component.TextFile
{
    public class MdTextWriter
    {
        public enum WritingType
        {
            WriteLines,

            WriteAllText,
        }

        public static String ToString(WritingType type)
        {
            switch (type)
            {
                case WritingType.WriteLines:
                    return "WriteLines";
                case WritingType.WriteAllText:
                    return "WriteAllText";
                default:
                    return "";
            }
        }

        public static WritingType ToWritingType(String value)
        {
            switch (value)
            {
                case "WriteLines":
                    return WritingType.WriteLines;
                case "WriteAllText":
                    return WritingType.WriteAllText;
                default:
                    return WritingType.WriteLines;
            }
        }
    }
}
