﻿using System;
using System.Text;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.TextFile
{
    internal interface StTextWriter_VI
        : DsEditForm_VI
    {
        Encoding Encoding { get; set; }

        MdTextWriter.WritingType WritingType { get; set; }
    }
}
